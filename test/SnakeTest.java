// *************		STUDENT NAME - Vivek Batra		*****************
// *************		STUDENT ID - 	C0741344		*****************


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest 
{
	// Lets Create two snakes
	Snake snake1;
	Snake snake2; 

	@Before
	public void setUp() throws Exception 
	{
		// Using constructor in Snake.java giving these snakes their own properties
		snake1 = new Snake("Peter", 10, "coffee");
		snake2 = new Snake("Takis", 80, "vegetables");
	}


	@Test
	public void testCase1() 
	{
		// checking if snake is healthy or not
		// using assertTrue and assertFalse functions to check the bollean value returned by the functions in Snake.java file
		// Snake will be healthy only if he like vegetables else he will not be healthy
		assertTrue(snake2.isHealthy());
		assertFalse(snake1.isHealthy());
		// Expected Out Put for snake 2 is True as he likes vegetables
		// Expected Out Put for snake 1 is false as he likes coffee
		
	}

	
	@Test
	public void testCase2() 
	{
		// checking if snake fits in cage or not
		// using assertTrue and assertFalse functions to check the bollean value returned by the functions in Snake.java file
		// snake will fit in cage only if his length is less than the length of cage.
		// if the length of cage will be more or equal to snake, he will not fit
		assertFalse(snake1.fitsInCage(4));
		assertFalse(snake1.fitsInCage(10));
		assertTrue(snake1.fitsInCage(22));
	}
}
