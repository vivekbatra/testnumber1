// *************		STUDENT NAME - Vivek Batra		*****************
// *************		STUDENT ID - 	C0741344		*****************


import static org.junit.Assert.*;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazeDemoTest 
{
	WebDriver driver;

	@Before
	public void setUp() throws Exception 
	{
		System.setProperty("webdriver.chrome.driver", "/Users/vivekbatra/Desktop/chromedriver");
		driver = new ChromeDriver();
		driver.get("http://blazedemo.com/");
				
	}

	@After
	public void tearDown() throws Exception 
	{
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void testCase1() 
	{
		List<WebElement> select = driver.findElements(By.name("fromPort"));
		WebElement firstSelect = select.get(0);
		List<WebElement> options = firstSelect.findElements(By.tagName("option"));
		int actualNumOptions = options.size();
		assertEquals(7, actualNumOptions);
		
	}
	
	@Test
	public void testCase2() 
	{
		WebElement findFlight = driver.findElement(By.tagName("input"));
		
		findFlight.click();
		
	}

}
